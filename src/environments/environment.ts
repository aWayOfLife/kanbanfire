// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyAFFPgychNGOltr9627rHUcpuLEHJyGD_0",
    authDomain: "kanbanfire-6a053.firebaseapp.com",
    databaseURL: "https://kanbanfire-6a053.firebaseio.com",
    projectId: "kanbanfire-6a053",
    storageBucket: "kanbanfire-6a053.appspot.com",
    messagingSenderId: "221082185015",
    appId: "1:221082185015:web:433684399d865ead53beb2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
